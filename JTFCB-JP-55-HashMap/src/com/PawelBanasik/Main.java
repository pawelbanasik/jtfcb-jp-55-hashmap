package com.PawelBanasik;

import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {

		HashMap<Integer, String> map = new HashMap<Integer, String>();

		map.put(5, "Five");
		map.put(8, "Eight");
		map.put(6, "Six");
		map.put(4, "Four");
		map.put(2, "Two");

		// nowszy wpis overrides the first one
		map.put(6, "Hello");

		String text = map.get(4);
		System.out.println(text);

		// itercja po wszystkich elementach zeby je wypisac
		// nie jest posortowana w zaden sposob
		// jak wypisujesz wartosci za drugim razem jak klikniesz run
		// moze juz tak nie byc
		for (Map.Entry<Integer, String> entry : map.entrySet()) {

			int key = entry.getKey();
			String value = entry.getValue();

			System.out.println(key + ": " + value);
		}
	}

}
